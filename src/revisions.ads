--------------------------------------------
-- Created 2025-02-13 18:54:23
--------------------------------------------
package revisions is
	dir : constant String := "/Users/rajasrinivasan/Prj/GitLab/codemd" ;
	version : constant String := "0.3.0" ;
	repo : constant String := "git@gitlab.com:ada23/codemd.git" ;
	commitid : constant String := "49554e799dec1d2403ce82ecd79269ad939883c0" ;
	abbrev_commitid : constant String := "49554e7" ;
	branch : constant String := "main" ;
end revisions ;
