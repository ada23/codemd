with Ada.Text_IO;           use Ada.Text_IO;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;

with GNAT.Regpat; use GNAT.Regpat;

with md;
package body impl is
   use GNAT.Strings;
   -- codemd: begin segment=patterns caption=Patterns for codemd markup
   cmdid      : constant String          := "codemd" & ":";
   beginpat   : constant Pattern_Matcher :=
     Compile (cmdid & " +begin" & " +segment=([a-zA-Z0-9]+)");
   endpat     : constant Pattern_Matcher := Compile (cmdid & " +end");
   skippat    : constant Pattern_Matcher := Compile (cmdid & " +skipbegin");
   skipendpat : constant Pattern_Matcher := Compile (cmdid & " +skipend");
   -- codemd: end

   strpat     : constant Pattern_Matcher := Compile ('"' & ".*" & '"');
   wordpat    : constant Pattern_Matcher := Compile ("[a-zA-Z]+");
   keywordpat : constant Pattern_Matcher :=
     Compile
       ("procedure[ \t$]|" & "begin[ \t$]|" & "end[ \t$]|" &
        "function[ \t$]|" & "with[ \t$]|" & "use[ \t$]|" & "constant[ \t$]|" &
        "string[ \t$]|" & "loop[ \t$]",
        GNAT.Regpat.Case_Insensitive);

   function ZeroPrefixed (val : Integer; length : Integer := 4) return String
   is
      valimg : constant String               := Integer'Image (val);
      pfx    : constant String (1 .. length) := (others => '0');
   begin
      return pfx (1 .. length - valimg'Length + 1) & valimg (2 .. valimg'Last);
   end ZeroPrefixed;

   procedure Emit_Line (lineno : Integer; line : String) is
   begin
      Put (ZeroPrefixed (lineno));
      Put (" | ");
      Put_Line (line);
   end Emit_Line;

   procedure Emit_Prolog
     (caption : GNAT.Strings.String_Access; inputfilename : String)
   is
   begin
      if caption.all'Length /= 0 then
         Put ("###### ");
         Put ("**" & caption.all & "**");
         Put ("{.unnumbered}");
         New_Line;
      end if;

      Put ("``` {filename=""");
      Put (inputfilename);
      Put (""" style=""background-color: beige; color:steel-blue""}");
      New_Line;
   end Emit_Prolog;

   procedure Emit_Epilog is
   begin
      Put_Line ("```");
   end Emit_Epilog;

   procedure Extract
     (inputfilename : String; segment : String := "*";
      caption       : GNAT.Strings.String_Access := null)
   is
      inpfile    : File_Type;
      inpline    : String (1 .. 512);
      inplinelen : Natural;
      lineno     : Integer := 0;
      segspec    : Match_Array (0 .. 2);

      procedure Skip_Segment is
      begin
         while not End_Of_File (inpfile) loop
            Get_Line (inpfile, inpline, inplinelen);
            lineno := lineno + 1;
            if Match (endpat, inpline (1 .. inplinelen)) then
               return;
            end if;
         end loop;
      end Skip_Segment;

      procedure Skip_Section is
      begin
         while not End_Of_File (inpfile) loop
            Get_Line (inpfile, inpline, inplinelen);
            lineno := lineno + 1;
            if Match (skipendpat, inpline (1 .. inplinelen)) then
               return;
            end if;
         end loop;
      end Skip_Section;

      function Transform (t : String) return String is
         Matches : Match_Array (0 .. 0);
         Current : Natural          := t'First;
         Result  : Unbounded_String := Null_Unbounded_String;
      begin
         loop
            -- Match (wordpat, t , Matches, Current );
            Match (keywordpat, t, Matches, Current);
            if Matches (0) = No_Match then
               Append (Result, t (Current .. t'Last));
               exit;
            end if;

            if Matches (0).First /= Current then
               Append (Result, t (Current .. Matches (0).First - 1));
               Append
                 (Result,
                  md.Transform
                    (t (Matches (0).First .. Matches (0).Last), md.Bold));
            end if;

            Current := Matches (0).Last + 1;
         end loop;
         return To_String (Result);
      end Transform;

      --codemd: begin segment=Segment caption=Process_Segment
      procedure Process_Segment is
      begin
         while not End_Of_File (inpfile) loop
            Get_Line (inpfile, inpline, inplinelen);
            lineno := lineno + 1;
            if Match (endpat, inpline (1 .. inplinelen)) then
               return;
            elsif Match (skippat, inpline (1 .. inplinelen)) then
               New_Line;
               Put_Line ("...skipped...");
               Skip_Section;
               New_Line;
            else
               Emit_Line (lineno, inpline (1 .. inplinelen));
            end if;
         end loop;
      end Process_Segment;
      --codemd: end
   begin
      --codemd: begin segment=Process
      --Put_Line(inputfilename);
      Open (inpfile, In_File, inputfilename);
      Emit_Prolog (caption, inputfilename);
      while not End_Of_File (inpfile) loop
         Get_Line (inpfile, inpline, inplinelen);
         lineno := lineno + 1;
         --codemd: skipbegin
         Match (beginpat, inpline (1 .. inplinelen), segspec);
         if segspec (0) /= No_Match then
            if segment = inpline (segspec (1).First .. segspec (1).Last) or
              segment = "*"
            then
               Process_Segment;
            else
               Skip_Segment;
            end if;
         end if;
         --codemd: skipend
      end loop;
      Close (inpfile);
      Emit_Epilog;
      --codemd: end
   end Extract;

   procedure Extract
     (inputfilename : String; lineno_from : Integer; linecount : Integer;
      caption       : GNAT.Strings.String_Access := null)
   is

      inpfile    : File_Type;
      inpline    : String (1 .. 512);
      inplinelen : Natural;
      lineno     : Integer := 0;
      count      : Integer := 0;

   begin
      Open (inpfile, In_File, inputfilename);
      Emit_Prolog (caption, inputfilename);
      while not End_Of_File (inpfile) loop
         Get_Line (inpfile, inpline, inplinelen);
         lineno := lineno + 1;
         if lineno >= lineno_from then
            Emit_Line (lineno, inpline (1 .. inplinelen));
            count := count + 1;
            if count >= linecount then
               exit;
            end if;
         end if;
      end loop;
      Close (inpfile);
      Emit_Epilog;
   end Extract;
end impl;
